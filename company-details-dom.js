export { showDetails, updateAverageTotalIncomeDOM }

function showDetails(selectedCompany) {
    document.getElementById('company-id').innerHTML = 'Id: ' + selectedCompany.id;
    document.getElementById('company-name').innerHTML = 'Name: ' + selectedCompany.name;
    document.getElementById('company-city').innerHTML = 'City: ' + selectedCompany.city;
    updateAverageTotalIncomeDOM(selectedCompany.totalIncome, selectedCompany.averageIncome);
    document.getElementById('company-last-month-income').innerHTML = 'Last Month Total Income: $' + selectedCompany.lastMonthTotalIncome;
}

function updateAverageTotalIncomeDOM(totalIncome, averageIncome) {
    document.getElementById('company-total-income').innerHTML = 'Total Income: $' + totalIncome;
    document.getElementById('company-average-income').innerHTML = 'Average Income: $' + averageIncome;
}