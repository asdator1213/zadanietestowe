export { generateTableRows };


function createTableRow(company, tBody) {
    const tableRow = document.createElement('tr');

    tableRow.addEventListener('click', onTableRowClick.bind(null, company));

    for (let value of Object.values(company)) {
        let td = document.createElement('td')
        td.innerHTML = value;
        tableRow.appendChild(td);
    }
    tBody.appendChild(tableRow);
}

function createTableBody(companies, currentPage, pageSize) {
    let tBody = document.createElement('tbody');
    tBody.setAttribute('id', 'companies-table-body');
    let companiesTable = document.getElementById('companies-table');
    companiesTable.appendChild(tBody);

    for (let i = 0 + currentPage * pageSize, counter = 0; i < companies.length; i++) {
        if (counter === pageSize)
            break;
        createTableRow(companies[i], tBody)
        counter++;
    }
}

function generateTableRows(companies, currentPage, pageSize) {
    if (companies instanceof Array) {
        if (document.getElementById('companies-table-body') !== null)
            document.getElementById('companies-table-body').remove()

        createTableBody(companies, currentPage, pageSize)
    }

}

function onTableRowClick(company) {
    let companyObj = {
        id: company.id,
        name: company.name,
        city: company.city,
        totalIncome: company.totalIncome
    }

    localStorage.setItem('company', JSON.stringify(companyObj))
    window.location.href = "company-details.html";
}