Aplikacja korzysta z biblioteki `live-server`, należy ją pobrać poleceniem `npm install -g live-server`

Po pobraniu repozytorium należy w terminalu przejść do folderu zadanietestowe i wywołać polecenie
`npm start`, które powinno uruchomić lokalny serwer na porcie 4200 z działającą aplikacją.