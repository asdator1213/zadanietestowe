export function drawChart(incomesPerMonth) {

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: incomesPerMonth.months,
            datasets: [{
                label: 'Income per Month',
                backgroundColor: '#1e90ff',
                borderColor: 'rgb(0, 0, 0)',
                data: incomesPerMonth.incomes
            }]
        },

        // Configuration options go here
        options: {}
    });
}