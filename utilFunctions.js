export {
    getCompaniesByName, getLastMonth, getIncomesOfLastMonth, getTotalIncomePerMonth,
    calculateLastMonthIncome, getTotalIncome, getAverageIncome
}


function getCompaniesByName(companies, nameFilter) {
    return companies.filter(item => item.name.toLowerCase()
        .includes(nameFilter.toLowerCase()));
}

function getLastMonth(incomes) {
    let lastMonth = incomes[0].date.substring(0, 7);

    for (let i = 1; i < incomes.length; i++) {
        if (incomes[i].date.substring(0, 7) > lastMonth)
            lastMonth = incomes[i].date.substring(0, 7)
    }
    return lastMonth;
}

function getIncomesOfLastMonth(lastMonthOfIncome, incomes) {
    const filteredIncomes = incomes.filter((elem) => {
        if (elem.date.includes(lastMonthOfIncome))
            return elem;
    })
    return filteredIncomes;
}

function getTotalIncomePerMonth(months, incomes) {
    let totalIncomePerMonth = []
    for (let month of months) {

        let incomesArr = incomes.filter(item => {
            if (item.date.includes(month))
                return item
        })
            .map((item) => parseFloat(item.value))

        const sumOfIncomes = incomesArr.reduce((sum, nextObject) => {
            return sum += parseFloat(nextObject)
        }, 0)

        totalIncomePerMonth.push(sumOfIncomes.toFixed(2));
    }
    return totalIncomePerMonth;
}

function calculateLastMonthIncome(lastMonthIncomes, lastMonth) {

    let lastMonthIncome = lastMonthIncomes.reduce((sum, nextObject) => {
        if (nextObject.date.includes(lastMonth)) {
            return sum += parseFloat(nextObject.value)
        }
    }, 0)
    return parseFloat(lastMonthIncome).toFixed(2)
}

function getTotalIncome(incomes) {
    let totalIncome = incomes.reduce((sum, nextObject) => {
        return sum += parseFloat(nextObject.value)
    }, 0)

    return isNaN(totalIncome) ? 0 : parseFloat(totalIncome).toFixed(2)
}

function getAverageIncome(totalIncome, incomesLength) {
    return totalIncome == 0 ? 0 : parseFloat(totalIncome / incomesLength).toFixed(2)
}