import { URL_ENDPOINT } from './endpoint.js';
import { updateAverageTotalIncomeDOM, showDetails } from './company-details-dom.js'
import { drawChart } from './chart.js'
import {
    getLastMonth, getIncomesOfLastMonth, calculateLastMonthIncome,
    getTotalIncome, getAverageIncome, getTotalIncomePerMonth
} from "./utilFunctions.js";

window.onload = function () {

    let incomes = [];
    let uniqueMonths = [];
    let totalIncomesPerMonth = [];

    function getCompany() {
        return JSON.parse(localStorage.getItem('company'));
    }

    function getIncomes(id) {
        fetch(`${URL_ENDPOINT}/incomes/${id}`)
            .then(response => {
                if (response.ok)
                    return response.json()
            })
            .then(data => {
                return new Promise((resolve, reject) => {
                    incomes = data.incomes;
                    uniqueMonths = getUniqueMonths([...incomes]);
                    totalIncomesPerMonth = getTotalIncomePerMonth(uniqueMonths, [...incomes])
                    selectedCompany.averageIncome = parseFloat(selectedCompany.totalIncome / incomes.length)
                        .toFixed(2);
                    resolve()
                })
            })
            .then(() => {
                return new Promise((resolve, reject) => {
                    const incomesArr = [...incomes];
                    const lastMonthOfIncome = getLastMonth(incomesArr);
                    const lastMonthIncomes = getIncomesOfLastMonth(lastMonthOfIncome, incomesArr);
                    selectedCompany.lastMonthTotalIncome = calculateLastMonthIncome(lastMonthIncomes, lastMonthOfIncome);
                    resolve()
                })
            })
            .then(() => {
                showDetails(selectedCompany)
                const incomesPerMonth = {
                    months: uniqueMonths,
                    incomes: totalIncomesPerMonth
                }

                drawChart(incomesPerMonth);
            })
            .catch(err => console.error(err))

    }

    this.document.getElementById('startDateInput').addEventListener('change', function () {
        const startDate = this.value;
        const endDate = document.getElementById('endDateInput').value;

        const incomesByDateRange = getIncomesByDateRange(startDate, endDate);
        selectedCompany.totalIncome = getTotalIncome(incomesByDateRange);
        selectedCompany.averageIncome = getAverageIncome(selectedCompany.totalIncome, incomesByDateRange.length)

        const incomesPerMonth = getDataForChart(incomesByDateRange, startDate, endDate);
        drawChart(incomesPerMonth);
        updateAverageTotalIncomeDOM(selectedCompany.totalIncome, selectedCompany.averageIncome);
    })

    this.document.getElementById('endDateInput').addEventListener('change', function () {
        const endDate = this.value;
        const startDate = document.getElementById('startDateInput').value;

        const incomesByDateRange = getIncomesByDateRange(startDate, endDate);
        selectedCompany.totalIncome = getTotalIncome(incomesByDateRange);
        selectedCompany.averageIncome = getAverageIncome(selectedCompany.totalIncome, incomesByDateRange.length)

        const incomesPerMonth = getDataForChart(incomesByDateRange, startDate, endDate)
        drawChart(incomesPerMonth);
        updateAverageTotalIncomeDOM(selectedCompany.totalIncome, selectedCompany.averageIncome);
    })


    function getDataForChart(incomesByDateRange, startDate, endDate) {
        uniqueMonths = getUniqueMonths(incomesByDateRange, startDate, endDate);
        totalIncomesPerMonth = getTotalIncomePerMonth(uniqueMonths, incomesByDateRange)
        return {
            months: uniqueMonths,
            incomes: totalIncomesPerMonth
        };
    }

    function getIncomesByDateRange(startDate, endDate) {

        const innerStartDate = startDate ? startDate : '0000-00';
        const innerEndDate = endDate ? endDate : new Date().toISOString().slice(0, 7);


        let filteredData = incomes.filter(item => {
            if (item.date >= innerStartDate && (item.date <= innerEndDate || item.date.includes(innerEndDate))) {
                return item;

            }
        })
        return filteredData;
    }

    function getUniqueMonths(incomes, startDate, endDate) {
        let months = [];

        const innerStartDate = startDate ? startDate : '0000-00';
        const innerEndDate = endDate ? endDate : new Date().toISOString().slice(0, 7);

        incomes.filter(obj => {
            if (obj.date >= innerStartDate && (obj.date <= innerEndDate || obj.date.includes(innerEndDate)))
                months.push(obj.date.substring(0, 7))
        })
        months.sort()
        return [...new Set(months)];
    }


    // function getTotalIncomePerMonth(months, incomes) {
    //     let totalIncomePerMonth = []
    //     for (let month of months) {

    //         let incomesArr = incomes.filter(item => {
    //             if (item.date.includes(month))
    //                 return item
    //         })
    //             .map((item) => parseFloat(item.value))

    //         const sumOfIncomes = incomesArr.reduce((sum, nextObject) => {
    //             return sum += parseFloat(nextObject)
    //         }, 0)

    //         totalIncomePerMonth.push(sumOfIncomes.toFixed(2));
    //     }
    //     return totalIncomePerMonth;
    // }




    ////
    let selectedCompany = getCompany();
    getIncomes(selectedCompany.id)

}


