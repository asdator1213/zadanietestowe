import { URL_ENDPOINT } from './endpoint.js';
import { generateTableRows } from "./companies-table.js";
import { getCompaniesByName } from "./utilFunctions.js";

window.onload = function () {

    let currentPage = 0
    let pageSize = 10;
    let companies = [];
    let nameFilter = '';

    function init() {
        document.getElementById('pageSize10').classList.add('active')

        fetch(`${URL_ENDPOINT}/companies`)
            .then(response => {
                if (response.ok)
                    return response.json()
            })
            .then((data) => {
                companies = data;
                let promises = [];

                for (let i = 0; i < companies.length; i++)
                    promises.push(fetch(`${URL_ENDPOINT}/incomes/${companies[i].id}`)
                        .then(data => data.json())
                    )

                return Promise.all(promises);
            })
            .then(data => {
                return new Promise((resolve, reject) => {
                    let totalIncome = 0;
                    for (let i = 0; i < data.length; i++) {
                        totalIncome = data[i].incomes.reduce((sum, nextObject) => {
                            return sum += parseFloat(nextObject.value);
                        }, 0)
                        companies[i].totalIncome = parseFloat(totalIncome).toFixed(2);
                    }
                    resolve()
                })
            })
            .then(() => {
                return new Promise((resolve, rej) => {
                    companies.sort((a, b) => {
                        return b.totalIncome - a.totalIncome;
                    })
                    resolve();
                })
            })
            .then(() => {
                generateTableRows(companies, currentPage, pageSize);
            })
            .catch(err => {
                console.error(err);
            })

    }



    ////////paginacja


    document.getElementById('pageSize10').addEventListener('click', function () {
        makePaginationButtonActive(this);
        pageSize = 10;
        currentPage = 0;
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })
    document.getElementById('pageSize30').addEventListener('click', function () {
        makePaginationButtonActive(this);
        pageSize = 30;
        currentPage = 0;
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })
    document.getElementById('pageSize50').addEventListener('click', function () {
        makePaginationButtonActive(this);
        pageSize = 50;
        currentPage = 0;
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })
    document.getElementById('pageSizeAll').addEventListener('click', function () {
        makePaginationButtonActive(this);
        pageSize = companies.length;
        currentPage = 0;
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })

    document.getElementById('nextPage').addEventListener('click', function () {
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        if ((currentPage + 1) * pageSize >= filteredCompanies.length)
            return;

        currentPage++;
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })

    document.getElementById('previousPage').addEventListener('click', function () {
        if (currentPage === 0)
            return;

        currentPage--;
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })


    function makePaginationButtonActive(clickedButton) {
        let paginationButtons = document.getElementsByClassName('pagination-btn');
        for (let i = 0; i < paginationButtons.length; i++)
            paginationButtons[i].classList.remove('active')
        clickedButton.className += ' active';
    }


    ///szukajka


    document.getElementById('search-bar-input').addEventListener('input', function () {
        currentPage = 0;
        nameFilter = this.value;
        let filteredCompanies = getCompaniesByName(companies, nameFilter);
        generateTableRows(filteredCompanies, currentPage, pageSize)
    })

    /////////////
    init();


}